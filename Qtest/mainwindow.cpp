﻿#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "../QtPlugin/plugininterface.h"
#include <QDir>
#include <QDebug>
#include <QPluginLoader>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QDir pluginsDir = QDir(qApp->applicationDirPath());

    pluginsDir.cd("plugins");

    foreach (QString fileName, pluginsDir.entryList(QDir::Files))
    {
        qDebug() << fileName;
        qDebug() << pluginsDir.absoluteFilePath(fileName);
        QPluginLoader loader(pluginsDir.absoluteFilePath(fileName));

        qDebug() << loader.loadHints();
        QObject *plugin = loader.instance();
        qDebug() << plugin;
        if (plugin)
        {
            PluginInterface *plugin_widget = qobject_cast<PluginInterface *>(plugin);
            if (plugin_widget)
            {
                this->setCentralWidget(plugin_widget->createWidget());
            }
        }
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}
