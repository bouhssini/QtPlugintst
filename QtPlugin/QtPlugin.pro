#-------------------------------------------------
#
# Project created by QtCreator 2015-03-03T20:37:23
#
#-------------------------------------------------

QT       += core gui
greaterThan(QT_MAJOR_VERSION, 4):QT += widgets webkitwidgets

TARGET = QtPlugin
TEMPLATE = lib
CONFIG += plugin
DESTDIR = ../bin/plugins

SOURCES += \
    widgetplugin.cpp \
    plugin.cpp

HEADERS += \
    widgetplugin.h \
    plugin.h \
    plugininterface.h
DISTFILES +=

FORMS += \
    widgetplugin.ui

