﻿#include "plugin.h"
 
Plugin::Plugin(QObject *parent) : QObject(parent)
{
}
 
QWidget * Plugin::createWidget()
{
	return new WidgetPlugin();
}
#if QT_VERSION < 0x050000
Q_EXPORT_PLUGIN2(NULL, Plugin) //(WidgetPlugin, Plugin)
#endif
