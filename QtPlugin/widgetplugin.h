﻿#ifndef WIDGETPLUGIN_H
#define WIDGETPLUGIN_H

#include <QWidget>

namespace Ui {
class WidgetPlugin;
}

class WidgetPlugin : public QWidget
{
    Q_OBJECT

public:
    explicit WidgetPlugin(QWidget *parent = 0);
    ~WidgetPlugin();

private slots:
    void on_pushButton_clicked();

private:
    Ui::WidgetPlugin *ui;
};

#endif // WIDGETPLUGIN_H
