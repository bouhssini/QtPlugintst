﻿#ifndef PLUGININTERFACE_H
#define PLUGININTERFACE_H

#include <QtPlugin>

class QWidget;


class PluginInterface
{
public:
    virtual ~PluginInterface() {}
    virtual QWidget* createWidget() = 0;
};

Q_DECLARE_INTERFACE(PluginInterface, "org.qt-project.Qt.PluginInterface/1.0")

#endif // PLUGININTERFACE_H
