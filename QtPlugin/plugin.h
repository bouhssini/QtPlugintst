﻿#ifndef PLUGIN_H
#define PLUGIN_H
 
#include <QObject>
#include <QWidget>
#include "plugininterface.h"
 
#include "widgetplugin.h"
 
class Plugin : public QObject, public PluginInterface
{
	Q_OBJECT
	Q_INTERFACES(PluginInterface)

    #if QT_VERSION >= 0x050000
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QtPlugin") // FILE "file.json")
    #endif

public:
	explicit Plugin(QObject *parent = 0);
	QWidget *createWidget();
 
signals:
 
public slots:
 
};

#endif // PLUGIN_H
